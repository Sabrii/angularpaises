import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';

import { PaisService } from '../../services/pais.service';
import { Pais } from '../../interface/pais.interface';

@Component({
  selector: 'app-por-pais',
  templateUrl: './por-pais.component.html',
  styleUrls: ['./por-pais.component.css']
})
export class PorPaisComponent {

  constructor(private paisService: PaisService) { }
  termino: string = ""
  hayError: boolean = false;
  paises: Pais[];
  mensaje: string = "";
  placeholder: string = "buscar pais"
  paisSugerido: Pais[] = []

  buscar(termino: string) {
    this.hayError = false
    this.termino = termino
    console.log(termino)
    this.paisService.buscarPais(this.termino)
      .subscribe((resp) => {
        this.paises = resp;
        console.log(resp)

      },
        (error) => {
          this.resultado(termino)
          this.hayError = true
          this.paises = []


        })


  }

  resultado(termino: string) {
    console.log("Hola")
    this.mensaje = `No se encontraron resultados de busqueda para ${termino}`
    console.log(this.mensaje)
  }

  sugerencia(termino: string) {
    // console.log(termino)
    if (!termino) {
      this.paisSugerido = []
      return
    }
    this.paisService.buscarPais(termino)
      .subscribe(resp => this.paisSugerido = resp.splice(0, 5)
        , (error) => {
          this.paisSugerido = []
        })


  }




}
