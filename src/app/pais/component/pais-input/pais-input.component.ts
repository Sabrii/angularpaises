import { Component, Output, EventEmitter, OnInit, Input } from '@angular/core';
import { Subject } from 'rxjs';
import { debounceTime } from 'rxjs/operators';


@Component({
  selector: 'app-pais-input',
  templateUrl: './pais-input.component.html',
  styleUrls: ['./pais-input.component.css']
})
export class PaisInputComponent implements OnInit{
  
  @Output() onEnter: EventEmitter<string> = new EventEmitter();
  @Output() onDebounce: EventEmitter<string> = new EventEmitter();


  @Input() placeholder: string
  termino: string =""

  hayError: boolean =false
  debauncer : Subject<string> = new Subject()

  ngOnInit() {
    // this.debauncer
    // .pipe(debounceTime(300))
    // .subscribe(valor =>{
    //   console.log("onDebounce "+valor)
    // })
    
  }

  buscar(){
    this.onEnter.emit(this.termino);
  }

  teclaPresionada(termino: string){
 // console.log("terminoooo"+this.termino)
    this.onDebounce.emit(this.termino)

  }

}
