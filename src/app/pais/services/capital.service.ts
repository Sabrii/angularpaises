import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Pais } from '../interface/pais.interface';

@Injectable({
  providedIn: 'root'
})
export class CapitalService {

  constructor(private http: HttpClient) { }

  private apiUrl = 'https://restcountries.eu/rest/v2'

  buscarCapital(termino :string ): Observable<Pais[]>{
    
    const url = `${this.apiUrl}/capital/${termino}`
    return this.http.get <Pais[]>(url)
  }
}
