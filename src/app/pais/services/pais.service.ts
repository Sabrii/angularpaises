import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Pais } from '../interface/pais.interface';


@Injectable({
  providedIn: 'root'
})
export class PaisService {

  constructor(private http: HttpClient) { }

  private apiUrl = 'https://restcountries.eu/rest/v2'

  buscarPais(termino :string ): Observable<Pais[]>{
    // console.log("el termino es "+ termino)
    const url = `${this.apiUrl}/name/${termino}`
    return this.http.get <Pais[]>(url)
  }

  buscarPaisxCodigo(id :string ): Observable<Pais>{
    // console.log("el termino es "+ termino)
    const url = `${this.apiUrl}/alpha/${id}`
    return this.http.get <Pais>(url)
  }

  buscarPorRegion(id :string ): Observable<Pais[]>{
    // console.log("el termino es "+ termino)
    const url = `${this.apiUrl}/region/${id}`
    return this.http.get <Pais[]>(url)
  }




}
