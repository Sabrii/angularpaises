import { Component, OnInit } from '@angular/core';
import { PaisService } from '../../services/pais.service';
import { Pais } from '../../interface/pais.interface';

@Component({
  selector: 'app-por-region',
  templateUrl: './por-region.component.html',
  styleUrls: ['./por-region.component.css']
})
export class PorRegionComponent implements OnInit {
  regiones: string[] = ["Africa", "Americas", "Asia", "Europe", "Oceania"];
  regionActiva: string =''
  paises: Pais[]
  constructor( private servicioPais: PaisService) { }

  ngOnInit() {
  }


  seleccionRegion(region: string){
    console.log("region"+region)
    this.regionActiva = region;
  }

  getClass(region: string ){
    return (region=== this.regionActiva) ? 'btn_seleccionado' : "btn_deseleccionado"

  }

  getPaisesxRegion(region: string){
    //if(region === this.regionActiva) return
    this.servicioPais.buscarPorRegion(region)
      .subscribe(resp=>{
        this.paises =resp;
        console.log("paises  por region son "+ resp)
      })

  }






}
