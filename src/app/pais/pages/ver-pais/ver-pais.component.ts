import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PaisService } from '../../services/pais.service';
import { switchMap, tap } from 'rxjs/operators';
import { Pais } from '../../interface/pais.interface';


@Component({
  selector: 'app-ver-pais',
  templateUrl: './ver-pais.component.html',
  styleUrls: ['./ver-pais.component.css']
})
export class VerPaisComponent implements OnInit {
  
  pais: Pais;
  
  constructor(
    private activateRoute: ActivatedRoute,
    private servicePais: PaisService
    ) { }

  ngOnInit() {
    this.activateRoute.params
      .pipe(
        switchMap(({id})=>  this.servicePais.buscarPaisxCodigo(id)),
        tap(console.log)
      ).subscribe((pais)=> this.pais = pais  )
    // this.activateRoute.params
    //   .subscribe(({id})=>{
    //     console.log(id)
    //     this.servicePais.buscarPaisxCodigo(id)
    //       .subscribe(pais=>{
    //         console.log(pais)
    //       })
    //   })
  }

}
