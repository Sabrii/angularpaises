import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { PaisModule } from './pais/pais.module';
import { ShareModule } from './share/share.module';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from "@angular/common/http";


@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    PaisModule,
    ShareModule
  ],

  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
