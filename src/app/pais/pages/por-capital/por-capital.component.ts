import { Component, OnInit } from '@angular/core';
import { Pais } from '../../interface/pais.interface';
import { CapitalService } from '../../services/capital.service';
import { PaisService } from '../../services/pais.service';

@Component({
  selector: 'app-por-capital',
  templateUrl: './por-capital.component.html',
  styleUrls: ['./por-capital.component.css']
})
export class PorCapitalComponent implements OnInit {

  constructor(private capitalService: CapitalService) { }

  ngOnInit() {
  }
  termino : string = ""
  hayError: boolean = false
  paises: Pais[] =[];
  placeholder: string = "buscar capital"

  buscar( termino: string){
    this.hayError = false
    this.termino = termino
    this.capitalService.buscarCapital(this.termino)
     .subscribe((resp )=>{
       this.paises = resp;
       console.log("resp"+resp)       
     },
     (error)=>{
       this.hayError = true
       this.paises = []

     })
   

 }

 sugerencia(){
   console.log("sugerencia")
 }
}
